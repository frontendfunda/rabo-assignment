Requirement:

Angular CSV Import Application.

Used Angular Material for View Manipulations.

Covered Unit Test Cases for Components, Services and Custome Pipe.

CI/CD - Deployed in AWS-s3 Bucket via Bitbucket Pipeline

Commands:

ng serve  - to run the appication

ng test - to run the unit test cases

AWS Deployed Url : http://myraboassignment.s3-website.us-east-2.amazonaws.com

Unit Test Coverage Report : 

![alt text](https://myraboassignment.s3.us-east-2.amazonaws.com/assets/Coverage-report.png)


Unit Test Report : 

![alt text](https://myraboassignment.s3.us-east-2.amazonaws.com/assets/unit-test-csv-import.png)



