import { TestBed } from '@angular/core/testing';

import { ReadCsvService } from './read-csv.service';

describe('ReadCsvService', () => {
  let service: ReadCsvService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReadCsvService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('csvParser method should be triggered wihh Array of Inputs', () => {
    const mockDataInput: any[] = [['First name","Sur name","Issue count","Date of birth"'], ['Theo","Jansen",5,"1978-01-02T00:00:00"'], ['Fiona","de Vries",7,"1950-11-12T00:00:00"'], ['Petra","Boersma",1,"2001-04-20T00:00:00"']];
    const mockTitle = ["Firstname", "Surname", "Issuecount", "Dateofbirth"];
    const mockResult = [{ Firstname: "Theo", Surname: "Jansen", Issuecount: "5", Dateofbirth: "1978-01-02T00:00:00" }, { Firstname: "Fiona", Surname: "de Vries", Issuecount: "7", Dateofbirth: "1950-11-12T00:00:00" }, { Firstname: "Petra", Surname: "Boersma", Issuecount: "1", Dateofbirth: "2001-04-20T00:00:00" }];
    service.csvParser(mockDataInput);
    expect(service.title).toEqual(mockTitle);
    expect(service.csvArray).toEqual(mockResult);
  });

});
