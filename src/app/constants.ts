export const csv = {
    HEADER: "Rabo Assignment",
    FOOTER: "Technologies : Angular 9, Angular Material 9, TypeScript, ES6, JavaScript, Karma, Jasmine Csv File Reader, CI/CD, BitBucket, Amazon web service(AWS)",
    UPLOAD_CSV:"Drag Drop / Click here to upload your Csv",
    UPLOAD_FILE_NAME:"Recently Uploaded File",
    CSV_ERROR: "Please drag drop / upload only csv file",
    CSV_LIST_DEFAULT:"Data is not Available - Please upload your csv"
 };