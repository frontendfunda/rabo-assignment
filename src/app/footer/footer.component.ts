import { Component, OnInit } from '@angular/core';
import { csv } from '../constants';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
technologies:string;
  constructor() { }

  ngOnInit(): void {
    this.technologies = csv.FOOTER;
  }

}
