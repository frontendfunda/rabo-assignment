import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dateFormat',
    pure: false
})
export class DateFormatPipe implements PipeTransform {
    count = 1;
    constructor() { }

    transform(value: string): any {

        var regex = new RegExp("([0-9]{4}[-](0[1-9]|1[0-2])[-]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[-](0[1-9]|1[0-2])[-][0-9]{4})");
        var dateOk = regex.test(value);
        if (dateOk) {
            const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            const dateObj = new Date(value);
            const month = monthNames[dateObj.getMonth()];
            const day = String(dateObj.getDate()).padStart(2, '0');
            const year = dateObj.getFullYear();
            const output = day + ', ' + month + ', ' + year;
            return output;
        } else {
            return value;
        }
    }

}