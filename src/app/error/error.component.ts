import { Component, OnInit } from '@angular/core';
import { ReadCsvService } from '../read-csv.service';
import { csv } from '../constants';


@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  csvUploadError: boolean
  errorText : string;

  constructor(private services: ReadCsvService) { }

  ngOnInit(): void {
    this.errorText = csv.CSV_ERROR;
    this.services.error.subscribe((error) => {
      this.csvUploadError = error ? error : false;
    })
  }

  ngOnDestroy() {
    this.services.error.unsubscribe();
  }

}
