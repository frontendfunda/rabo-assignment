import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReadCsvService {

  // Communication between Csv upload componant to Csv List Componant
  csvData = new EventEmitter<string | ArrayBuffer>();

  // Communication between Csv upload componant to error Componant
  error = new EventEmitter<boolean>();

  // Json generator Input
  title: string[];
  csvArray: any[] = [];

  constructor() { }

  csvParser(value: string[]) {
    value.forEach((element, index) => {
      if (index === 0) {

        // This is Array of Titles.
        this.title = element.toString().replace(/"/g, '').replace(/\s/g, '').split(',');
        
      } else {

        // To set Key and Value : Json generator.
        const y = element.toString().replace(/"/g, '').split(",");
        const result: object = {};
        this.title.forEach((key, i) => result[key] = y[i]);
        this.csvArray.splice(index - 1, 0, result);
      }
    });
    return this.csvArray;
  }
}
