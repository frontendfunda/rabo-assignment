import { Component, OnInit } from '@angular/core';
import { csv } from '../constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  headerLogoTest: string;
  constructor() { }

  ngOnInit(): void {
    this.headerLogoTest = csv.HEADER;
  }

}
