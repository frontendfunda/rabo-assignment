import { DateFormatPipe } from './date.pipe';
let pipe;
describe('Pipe: MyPipe', () => {
    beforeEach(() => {
        pipe = new DateFormatPipe();
    });

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    it('Should triger pipe method', () => {
        const mockValue = "1978-01-02T00:00:00";
        //let pipe = new DateFormatPipe();
        pipe.transform(mockValue);
        expect(pipe.transform(mockValue)).toEqual("02, Jan, 1978");
    });
});