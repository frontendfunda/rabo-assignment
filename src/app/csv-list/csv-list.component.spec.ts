import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { By } from "@angular/platform-browser";
import { CsvListComponent } from './csv-list.component';
import { ReadCsvService } from '../read-csv.service';
import { EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Sort } from '@angular/material/sort';
import { DateFormatPipe } from '../date.pipe';


describe('CsvListComponent', () => {
  let component: CsvListComponent;
  let fixture: ComponentFixture<CsvListComponent>;
  let service: ReadCsvService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CsvListComponent, DateFormatPipe],
      imports: [
        BrowserAnimationsModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule],
      providers: [{ provide: ReadCsvService, useCase: mockServices }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvListComponent);
    component = fixture.componentInstance;
    service = TestBed.get(ReadCsvService);
    const MockResult = [{ Firstname: "Theo", Surname: "Jansen", Issuecount: "5", Dateofbirth: "1978-01-02T00:00:00" }, { Firstname: "Fiona", Surname: "de Vries", Issuecount: "7", Dateofbirth: "1950-11-12T00:00:00" }, { Firstname: "riona", Surname: "de Vries", Issuecount: "1", Dateofbirth: "1950-11-12T00:00:00" }];
    component.csvData = MockResult;
    component.displayedColumns = ["Firstname", "Surname", "Issuecount", "Dateofbirth"];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check the initializations value', () => {
    expect(component.length).toEqual(0);
    expect(component.pageIndex).toEqual(0);
    expect(component.pageSize).toEqual(10);
    expect(component.pageSizeOptions.length).toEqual(5);

  });

  it('Should trigger OnInit method', () => {

    const MockResult = [{ Firstname: "Theo", Surname: "Jansen", Issuecount: "5", Dateofbirth: "1978-01-02T00:00:00" }, { Firstname: "Fiona", Surname: "de Vries", Issuecount: "7", Dateofbirth: "1950-11-12T00:00:00" }, { Firstname: "riona", Surname: "e Vries", Issuecount: "1", Dateofbirth: "1950-11-12T00:00:00" }];
    component.ngOnInit();
    fixture.detectChanges();
    service.csvData.subscribe(() => {
      const mockDataInput: any[] = [['First name","Sur name","Issue count","Date of birth"'], ['Theo","Jansen",5,"1978-01-02T00:00:00"'], ['Fiona","de Vries",7,"1950-11-12T00:00:00"'], ['Petra","Boersma",1,"2001-04-20T00:00:00"']];
      const mockResult = [{ Firstname: "Theo", Surname: "Jansen", Issuecount: "5", Dateofbirth: "1978-01-02T00:00:00" }, { Firstname: "Fiona", Surname: "de Vries", Issuecount: "7", Dateofbirth: "1950-11-12T00:00:00" }, { Firstname: "Petra", Surname: "Boersma", Issuecount: "1", Dateofbirth: "2001-04-20T00:00:00" }];
      service.csvParser(mockDataInput)
      expect(component.csvData).toEqual(mockResult);
      expect(component.length).toEqual(MockResult.length);
    })
    expect(service.csvData.subscribe).toBeTruthy();
  });

  it('Should trigger OnPageLoad method', () => {
    let e: any = { "length": 10, "pageIndex": 1, "pageSize": 100 };
    spyOn(component, 'loadData');
    component.onPageChange(e);
    fixture.detectChanges();
    expect(component.pageIndex).toEqual(1);
    expect(component.pageSize).toEqual(100);
    expect(component.loadData).toHaveBeenCalledWith(component.pageIndex, component.pageSize);

  });

  it('Should trigger LoadData method', () => {
    let e: any = { "length": 10, "pageIndex": 0, "pageSize": 100 };
    component.loadData(e.pageIndex, e.pageSize);
    fixture.detectChanges();
    const noDataElement = fixture.debugElement.queryAll(By.css('.mat-row'));
    expect(noDataElement.length).toEqual(3);
  });

  it('Should trigger LoadData method with filter', () => {
    component.filterList = ["5"];
    let e: any = { "length": 10, "pageIndex": 0, "pageSize": 100 };
    component.loadData(e.pageIndex, e.pageSize);
    fixture.detectChanges();
    const noDataElement = fixture.debugElement.queryAll(By.css('.mat-row'));
    expect(noDataElement.length).toEqual(1);

  });

  it('Should trigger getFilterData method with filter', () => {
    component.pageIndex = 0;
    component.pageSize = 10;
    component.toppings = new FormControl();
    component.toppings.setValue(["5"]);
    component.getFilterData(false);
    fixture.detectChanges();
    const noDataElement = fixture.debugElement.queryAll(By.css('.mat-row'));
    expect(noDataElement.length).toEqual(1);

  });

  it('Should trigger getFilterData method without filter', () => {
    component.pageIndex = 0;
    component.pageSize = 10;
    component.toppings = new FormControl();
    component.toppings.setValue([]);
    component.getFilterData(false);
    fixture.detectChanges();
    const noDataElement = fixture.debugElement.queryAll(By.css('.mat-row'));
    expect(noDataElement.length).toEqual(3);

  });



  it('Should trigger getFilterData method without filter Firstname', () => {
    const sort: Sort = { active: "Firstname", direction: "asc" };
    component.filterList = [];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[0].innerHTML).toEqual(" Theo ");
  });
  it('Should trigger getFilterData method with filter Firstname', () => {
    const sort: Sort = { active: "Firstname", direction: "desc" };
    component.filterList = ["7"];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[0].innerHTML).toEqual(" Fiona ");
  });

  it('Should trigger getFilterData method without filter Surname ', () => {
    const sort: Sort = { active: "Surname", direction: "asc" };
    component.filterList = [];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[1].innerHTML).toEqual(" Jansen ");
  });
  it('Should trigger getFilterData method with filter Surname', () => {
    const sort: Sort = { active: "Surname", direction: "desc" };
    component.filterList = ["7"];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[1].innerHTML).toEqual(" de Vries ");
  });

  it('Should trigger getFilterData method without filter Issue count ', () => {
    const sort: Sort = { active: "Issuecount", direction: "asc" };
    component.filterList = [];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[2].innerHTML).toEqual(" 5 ");
  });
  it('Should trigger getFilterData method with filter Issue count', () => {
    const sort: Sort = { active: "Issuecount", direction: "desc" };
    component.filterList = ["7"];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[2].innerHTML).toEqual(" 7 ");
  });
  it('Should trigger getFilterData method without filter Date of birth ', () => {
    const sort: Sort = { active: "Dateofbirth", direction: "asc" };
    component.filterList = [];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[3].innerHTML).toEqual(" 02, Jan, 1978 ");
  });
  it('Should trigger getFilterData method with filter Date of birth', () => {
    const sort: Sort = { active: "Dateofbirth", direction: "desc" };
    component.filterList = ["7"];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[3].innerHTML).toEqual(" 12, Nov, 1950 ");
  });

  it('Should trigger getFilterData method with filter default value', () => {
    const sort: Sort = { active: "default", direction: "desc" };
    component.filterList = ["7"];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[3].innerHTML).toEqual(" 12, Nov, 1950 ");
  });

  it('Should trigger getFilterData method with filter no sorting', () => {
    const sort: Sort = { active: "", direction: "" };
    component.filterList = ["7"];
    component.pageIndex = 0;
    component.pageSize = 10;
    component.loadData(0, 10);
    component.sortData(sort);
    fixture.detectChanges();
    const noDataElement = fixture.nativeElement.querySelectorAll('tr');
    expect(noDataElement[1].cells[3].innerHTML).toEqual(" 12, Nov, 1950 ");
  });
});

class mockServices {
  csvData = new EventEmitter<string | ArrayBuffer>();
}
