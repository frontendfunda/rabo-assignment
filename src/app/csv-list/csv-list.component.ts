import { Component, OnInit } from '@angular/core';
import { ReadCsvService } from '../read-csv.service';
import { MatTableDataSource } from '@angular/material/table';
import { Sort } from '@angular/material/sort';
import { PageEvent } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { CsvDataList } from '../csv.model';
import { csv } from '../constants';


@Component({
  selector: 'app-csv-list',
  templateUrl: './csv-list.component.html',
  styleUrls: ['./csv-list.component.scss']
})
export class CsvListComponent implements OnInit {

  defaultTxt:string;

  // Filter Inputs
  toppings = new FormControl();
  toppingList: string[];

  // MatTabl Inputs
  displayedColumns: string[];
  dataSource: MatTableDataSource<CsvDataList>;

  csvData: any[];
  sortedData: CsvDataList[];
  filterList: any[];

  // MatPaginator Inputs
  length = 0;
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  constructor(private services: ReadCsvService) {
  }

  // MatPaginator Method.
  onPageChange(e: any) {
    this.pageIndex = e.pageIndex;
    this.pageSize = e.pageSize;
    this.loadData(this.pageIndex, this.pageSize);
  }
  // MatPaginator Method.
  loadData(pageIndex: number, pageSize: number) {

    // If no filter is choosed set the default value which includeds pagination alone.
    if (!this.filterList || this.filterList.length === 0) {
      this.length = this.csvData.length;
      this.dataSource = new MatTableDataSource(this.csvData.slice(pageIndex, pageIndex + pageSize));
      return;
    }

     // To get the filtered values as per the filter choosed.
    const filterData = this.csvData.slice().filter(csvData => this.filterList.includes(csvData.Issuecount));

    this.length = filterData.length;

    // To set the value of datascource based on filters and paginations value.
    this.dataSource = new MatTableDataSource(filterData.slice(pageIndex, pageIndex + pageSize));

  }

  ngOnInit() {

    this.defaultTxt = csv.CSV_LIST_DEFAULT;

    // Csv Data Fetch via Services
    this.services.csvData.subscribe(
      (csvResult: string) => {
        const textFromFileLoaded = csvResult;
        const csv = [];
        const lines = textFromFileLoaded.split(/\r\n|\n/);
        lines.forEach((element: any) => {
          const cols: string[] = element.split(';');
          csv.push(cols);
        });
        this.csvData = this.services.csvParser(csv);
        this.length = this.csvData.length;

        // Set Unique value for Filter option
        const fiterList = [];
        this.csvData.forEach((csvData) => { fiterList.push(csvData.Issuecount); });
        this.toppingList = [... new Set(fiterList)];

        // To display the column in View
        this.displayedColumns = this.services.title;

        // To update the Pagination
        this.loadData(this.pageIndex, this.pageSize);
      }
    );

  }

  ngOnDestroy() {
    this.services.csvData.unsubscribe();
  }

  // Issue Count Filter method.
  getFilterData(e: any) {
    if (!e) {
      this.filterList = this.toppings.value;

      // If no filter is choosed set the default value which includeds pagination alone.
      if (!this.filterList || this.filterList.length === 0) {
        this.length = this.csvData.length;
        this.dataSource = new MatTableDataSource(this.csvData.slice(this.pageIndex, this.pageIndex + this.pageSize));
        return;
      }
      // To get the filtered values as per the filter choosed.
      const filterData = this.csvData.slice().filter(csvData => this.filterList.includes(csvData.Issuecount));
      
      this.length = filterData.length;

      // To set the value of datascource based on filters and paginations value.
      this.dataSource = new MatTableDataSource(filterData.slice(this.pageIndex, this.pageIndex + this.pageSize));
    }
  }

  // Sorting option for all the colomn.
  sortData(sort: Sort) {
    const data = this.csvData.slice();
    if (!sort.active || sort.direction === '') {
      this.getFilterData(this.filterList);
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      if (sort.active) {
        const sortTo = sort.active;
        return this.compare(a.sortTo, b.sortTo, isAsc)
      }
      else {
        return 0;

      }
    });

    // If no filter is choosed set the default value which includeds pagination alone.
    if (!this.filterList || this.filterList.length === 0) {
      this.length = this.sortedData.length;
      this.dataSource = new MatTableDataSource(this.sortedData.slice(this.pageIndex, this.pageIndex + this.pageSize));
      return;
    }

    // To get the filtered values as per the filter choosed.
    const filterData = this.sortedData.slice().filter(csvData => this.filterList.includes(csvData.Issuecount));
    
    this.length = filterData.length;

     // To set the value of datascource based on filters and paginations value.
    this.dataSource = new MatTableDataSource(filterData.slice(this.pageIndex, this.pageIndex + this.pageSize));
  }

  // Returns asc or dec sort option
  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}

