import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CsvUploadComponent } from './csv-upload/csv-upload.component';
import { ReadCsvService } from './read-csv.service';
import { CsvListComponent } from './csv-list/csv-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { DateFormatPipe } from './date.pipe';
import { ErrorComponent } from './error/error.component';
import { DragDropDirective } from './drag-drop.directive';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CsvUploadComponent,
    CsvListComponent,
    DateFormatPipe,
    ErrorComponent,
    DragDropDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatToolbarModule,
    MatIconModule
  ],
  providers: [ReadCsvService],
  bootstrap: [AppComponent]
})
export class AppModule { }
