import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CsvUploadComponent } from './csv-upload.component';
import { ReadCsvService } from '../read-csv.service';
import { EventEmitter } from '@angular/core';


describe('CsvUploadComponent', () => {
  let component: CsvUploadComponent;
  let fixture: ComponentFixture<CsvUploadComponent>;
  let service: ReadCsvService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CsvUploadComponent],
      providers: [{ provide: ReadCsvService, useCase: mockServices }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvUploadComponent);
    component = fixture.componentInstance;
    service = TestBed.get(ReadCsvService);
    fixture.detectChanges();
  });

  it('should create Componant', () => {
    expect(component).toBeTruthy();
  });

  it('Should trigger Upload method', () => {

    const mockCSV = `Firstname,Surname Issuecount,Dateofbirth
Homer Simpson,5551234422,homer@springfield.com
Seymour Skinner,1235663322,a@b.c
Bart Simpson,2675465026,bart@spring.field
Montgomery Burns,2233459922,hi@bye.cya
Mayor Quimby,2222222222,mayor@springfield.gov
Waylon Smithers,3333333333,ok@hey.bye
Barney Gumble,111111111111,barney@gumble.gum
Marge Simpson,2627338461,marge@springfield.com
Edna Krabappel,2656898220,a@b.c
Lisa Simpson,2222222222,lisa@bix.com
Maggie Simpson,2716017739,maggie@spring.field
Linel Hutz,2745577499,hire@now.me
Troy McClure,2314928822,troy@acting.now
Rainer Wolfcastle,2221114455,rainer@acting.now
Krusty Clown,2321221188,krusty@acting.now
`;
    const el: any = new File([mockCSV], "simpsons.csv", { type: 'application/vnd.ms-excel' });
    spyOn(service.csvData, 'emit');
    el.files = [{ name: 'csv' }];
    component.csvUploadFile(el);
    fixture.detectChanges();
    component.fileDetail = el.files[0];
    component.files = mockCSV;
    const eventListener = jasmine.createSpy();
    const dummyFileReader = { addEventListener: eventListener };
    spyOn<any>(window, "FileReader").and.returnValue(dummyFileReader);
    const reader = new FileReader();
    reader.addEventListener('load', function (e) {
      expect(e.target.result).toEqual('url');
      expect(service.csvData.emit).toHaveBeenCalled();
    });
  });
});
class mockServices {
  csvData = new EventEmitter<string | ArrayBuffer>();
}

