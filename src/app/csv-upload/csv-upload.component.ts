import { Component, OnInit } from '@angular/core';
import { ReadCsvService } from '../read-csv.service';
import { csv } from '../constants';


@Component({
  selector: 'app-csv-upload',
  templateUrl: './csv-upload.component.html',
  styleUrls: ['./csv-upload.component.scss']
})
export class CsvUploadComponent implements OnInit {
  uploadText: string;
  uploadedFileName: string;

  // File upload Inputs
  files: any = [];
  fileDetail: any;


  constructor(private services: ReadCsvService) { }

  ngOnInit(): void {
    this.uploadText = csv.UPLOAD_CSV;
    this.uploadedFileName = csv.UPLOAD_FILE_NAME
  }

  // File upload Method
  csvUploadFile(input: HTMLInputElement) {
    this.files = input;
    if (this.files[0].name.indexOf('.csv') === -1 && this.files[0].type !== "application/vnd.ms-excel") {
      this.services.error.emit(true);
      return;
    }
    else {
      if (this.files && this.files.length) {
        this.fileDetail = this.files[0];
        const fileToRead = this.files[0];
        const fileReader = new FileReader();
        this.services.error.emit(false);
        fileReader.onload = (e) => {
          this.services.csvData.emit(e.target.result);
        };
        fileReader.readAsText(fileToRead, 'UTF-8');
      }
    }
  }
}
